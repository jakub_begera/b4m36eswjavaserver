package cz.jbegera.b4m36eswjavaserver;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Copyright 2017 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
public class WordStorage {

    private ConcurrentHashMap<String, Integer> map;

    public WordStorage() {
        this.map = new ConcurrentHashMap<String, Integer>();
    }

    public void addWord(String s) {
        map.put(s, 1);
    }

    public int getSize() {
        return map.size();
    }

    public void clear() {
        map.clear();
    }
}
