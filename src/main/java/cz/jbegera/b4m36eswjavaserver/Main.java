package cz.jbegera.b4m36eswjavaserver;

/**
 * Copyright 2017 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
public class Main {

    public static void main(String[] args) throws Exception {

        // read arguments
        if (args.length < 1) {
            System.err.println("Not enough arguments given. [port]");
            System.exit(1);
        }

        int port = Integer.parseInt(args[0]);

        WebServer webServer = new WebServer(port);

        webServer.run();
    }

}
