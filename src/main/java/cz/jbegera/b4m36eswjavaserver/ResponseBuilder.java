package cz.jbegera.b4m36eswjavaserver;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;

import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

/**
 * Copyright 2015 IEAP CTU
 * Author: Jakub Begera (jakub.begera@utef.cvut.cz)
 */
public class ResponseBuilder {

    public static FullHttpResponse buildOkResponse(HttpResponseStatus responseStatus) {
        ByteBuf msg = Unpooled.copiedBuffer("Success: " + responseStatus + "\r\n", CharsetUtil.UTF_8);
        FullHttpResponse out = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, responseStatus, msg);
        out.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8");
        return out;
    }

    public static FullHttpResponse buildErrorResponse(HttpResponseStatus responseStatus) {
        ByteBuf msg = Unpooled.copiedBuffer("Failure: " + responseStatus + "\r\n", CharsetUtil.UTF_8);
        FullHttpResponse out = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, responseStatus, msg);
        out.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8");
        return out;
    }

    public static FullHttpResponse buildResponseTextPlain(String text) {

        byte[] bytes = text.getBytes();

        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK);
        response.headers().setContentLength(response, bytes.length);
        response.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8");
        response.content().writeBytes(bytes);

        return response;
    }

}
