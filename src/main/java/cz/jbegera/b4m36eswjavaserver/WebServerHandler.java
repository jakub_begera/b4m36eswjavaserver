package cz.jbegera.b4m36eswjavaserver;

import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.zip.GZIPInputStream;

/**
 * Copyright 2017 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
public class WebServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    private final static String PATH_DATA = "/esw/myserver/data";
    private final static String PATH_COUNT = "/esw/myserver/count";


    private final WordStorage wordStorage;

    public WebServerHandler(WordStorage wordStorage) {
        this.wordStorage = wordStorage;
    }

    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) throws Exception {

        String uri = msg.getUri();

        if (msg.getMethod() == HttpMethod.POST && PATH_DATA.equals(uri)) {
            handleDataAddRequest(ctx, msg);
        } else if (msg.getMethod() == HttpMethod.GET && PATH_COUNT.equals(uri)) {
            handleGetCountRequest(ctx, msg);
        } else {
            writeResponse(ctx, ResponseBuilder.buildErrorResponse(HttpResponseStatus.NOT_FOUND));
        }

    }

    private void handleDataAddRequest(ChannelHandlerContext ctx, FullHttpRequest msg) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                new GZIPInputStream(new ByteBufInputStream(msg.content()))));


        String line;
        while ((line = reader.readLine()) != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(line);
            while (stringTokenizer.hasMoreTokens()) {
                wordStorage.addWord(stringTokenizer.nextToken());
            }
        }

        reader.close();

        writeResponse(ctx, ResponseBuilder.buildOkResponse(HttpResponseStatus.OK));
    }

    private void handleGetCountRequest(ChannelHandlerContext ctx, FullHttpRequest msg) throws Exception {
        int count = wordStorage.getSize();
        wordStorage.clear();
        String response = String.format("%d\n", count);
        writeResponse(ctx, ResponseBuilder.buildResponseTextPlain(response));
    }

    private void writeResponse(ChannelHandlerContext channelHandlerContext, FullHttpResponse response) {
        channelHandlerContext.write(response);
        channelHandlerContext.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);
    }
}
